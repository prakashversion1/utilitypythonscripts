import requests

def deerwalkDRM(userName,password,command):
    print("Writing into deermine")
    drmUrl = "https://drm.deerwalk.com/j_spring_security_check"
    redirect = "/"
    j_user_name = userName
    hostURL = "sdemo.makalu.qc.deerwalk.com"
    j_password = password
    button = "LoginName"
    with requests.session() as c:
        login_data = {"spring-security-redirect":redirect,"j_username":j_user_name,"hostUrl":hostURL,"j_password":j_password,"button":button}
        r = c.post(drmUrl,data=login_data)
        print (r.url) # this prints drm dashboard url https://drm.deerwalk.com/hrm/dashboard/index
        if(command == "login"):
            print "now start to login"
        else:
            print "logout"

def deerminOpener(userName,password,command):
    print "opening deermine"

if __name__ == '__main__':
    deerwalkDRM("pcjoshi","Admins#2","login")